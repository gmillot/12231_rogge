#!/bin/bash

################ INITIALIZATION


# echo -e "\nJOB COMMAND EXECUTED:\n$0\n" # to get the line that executes the job but does not work (gives /bioinfo/guests/gmillot/Gael_code/workflow_fastq_gael.sh)
# BEWARE: double __ is a reserved character string to deal with spaces in paths
module purge
LOCAL_USER_VAR= # empty variable that will be used to recover the name of all the variables created here
LANG=en_us_8859_1 # to use for the correct use of the date function
INI_POSITION=$(pwd)
LOCAL_USER_VAR+=" LANG INI_POSITION" # do not forget the space before the variable name
shopt -s expand_aliases # to be sure that alias are expended to the different environments
# set -e # stop the workflow if a command return something different from 0 in $?. Use set -ex to have the blocking line. It is important to know that each instruction executed send a result in $? Either 0 (true) or something else (different from 0)


################ SCRIPT NAME RECOVERY


SCRIPT_USED=$0 # SCRIPT_USED="" or SCRIPT_USED= (it is the same) produce an empty variable. SCRIPT_USED=`echo "$@"` keep only the options, not the .sh before the options ($0).
for i in $@ ; do
    SCRIPT_USED="$SCRIPT_USED $i"
done
SCRIPT_USED=$(echo $SCRIPT_USED | sed 's/\ /__/g') # replace space by 2 underscores in SCRIPT_USED
LOCAL_USER_VAR+=" SCRIPT_USED" # do not forget the space before the variable name


################ FUNCTIONS

function verif_fun {
    if [[ $1 = -* ]]; then
    ((OPTIND--))
    fi
}

function single_path_with_regex_fun { # comes from little_bash_functions-v1.0.0/little_bash_functions-v1.0.0.sh
    # DESCRIPTION
        # check that $1 path exists and is single 
    # BEWARE 
        # do not use set -e !
    # ARGUMENTS
        # $0: name of the function
        # $1: first argument, ie path with regex
    # RETURN OUTPUT
        # 0: single path detected is valid
        # 1: error: $1 not provided
        # 2: error: $2 provided or more than one path detected
        # 3: single url detected does not exist
        # 4: single path detected does not exist
    # EXAMPLES
        # single_path_with_regex_fun /cygdrive/c/Users/Gael/Desktop/config_tars_lodscore_gael_2017121[[:digit:]].conf
        # single_path_with_regex_fun /pasteur/homes/gmillot/dyslexia/code_gael/config_tars_lodscore_gael_2017121[[:digit:]].conf
    # argument checking
    if [[ -z $1 ]] ; then
        echo -e "\n### ERROR ###  ARGUMENT 1 REQUIRED IN single_path_with_regex_fun\n"
        return 1
    fi
    # main code
    if [[ ! -z $2 ]] ; then
        echo -e "\n### ERROR ###\n1) AT LEAST ARGUMENT 2 PRESENT IN single_path_with_regex_fun WHILE ONLY ONE REQUIRED OR\n2) REGEX PATH RESULT IN SEVERAL RESULTS:\n$@\n"
        return 2
    fi
    local ARG1="$1"
    # echo $ARG1 # print ARG1
    local ARG1_ARR=("$ARG1") # conversion to array (split acccording to space). If no corresponding regexp, keep $1 with regexp and length = 1
    # echo ${ARG1_ARR[@]} # print the array
    local ARG1_ARR_LENGTH=$(( ${#ARG1_ARR[@]} - 1 )) # total number of elements in the array
    # echo "LENGTH: $ARG1_ARR_LENGTH" # print the length of the array
    if [[ $ARG1_ARR_LENGTH != 0 ]] ; then
        echo -e "\n### ERROR ### SPECIFIED REGEXP PATH IN single_path_with_regex_fun CORRESPOND TO MORE THAN ONE PATH: ${ARG1_ARR_LENGTH[@]}\n";
        return 2
    else
        shopt -s extglob # -s unable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
        if [[  $(echo ${ARG1_ARR[0]} | grep -cE '^http' ) == 1 ]] ; then # -cE to specify extended and -c to return the number of match (here 0 or one only)
            if [[ $(wget ${ARG1_ARR[0]} >/dev/null 2>&1 ; echo $?) != 0 ]] ; then # check the valid url. wget $url >/dev/null 2>&1 prevent any action and print. echo $? print the result of the last command (0 = success, other number = failure)
                echo -e "\n### ERROR ### SPECIFIED URL IN single_path_with_regex_fun DOES NOT EXISTS: ${ARG1_ARR[0]}\n";
                shopt -u extglob # -u disable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
                return 3
            fi
        elif [[ ! ( -d ${ARG1_ARR[0]} || -f ${ARG1_ARR[0]} ) ]] ; then
            echo -e "\n### ERROR ### SPECIFIED PATH IN single_path_with_regex_fun DOES NOT EXISTS: ${ARG1_ARR[0]}\n";
            shopt -u extglob # -u disable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
            return 4
        else
            shopt -u extglob # -u disable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
            return 0
        fi
    fi
}

function var_existence_check_fun { #comes from little_bash_functions-v1.0.0/little_bash_functions-v1.0.0.sh
    # DESCRIPTION
        # check that $1 variable exists
        # $0: name of the function
        # $1: first argument, ie variable name without $
    # BEWARE 
        # do not use set -e !
    # RETURN OUTPUT
        # 0: variable is detected in the environment
        # 1: error: $1 not provided
        # 2: error: $2 provided or more than one path detected
        # 3: variable does not exist
    # EXAMPLES
        #
    # argument checking
    if [[ -z $1 ]] ; then
        echo -e "\n### ERROR ###  ARGUMENT 1 REQUIRED IN var_existence_check_fun\n"
        return 1
    fi
    # main code
    if [[ ! -z $2 ]] ; then
        echo -e "\n### ERROR ###\n1) AT LEAST ARGUMENT 2 PRESENT IN var_existence_check_fun WHILE ONLY ONE REQUIRED OR\n2) REGEX PATH RESULT IN SEVERAL RESULTS:\n$@\n"
        return 2
    fi
    ARG1=$(echo $1) 
    ARG2=$(echo ${!ARG1}) # double because I need to know if the content of $(echo $1) is "" (echo return "" when does not exist)
    if [[ -z "$ARG2" ]] ; then # test if $ARG1 is ""
        return 3
    else
        return 0
    fi
}



################ STARTING TIME JOB & JOB ID

STARTTIME=$(date)
JOB_ID=$(date -d "$STARTTIME" +%s)
LOCAL_USER_VAR+=" STARTTIME JOB_ID" # do not forget the space before the variable name
echo -e "\n----WORKFLOW JOB ${JOB_ID}----\n"
echo -e "PROCESS IGNITION START TIME:" $STARTTIME "\n"
TEMPO_SCRIPT_USED=$(echo $SCRIPT_USED | sed 's/__/\ /g') # replace 2 underscores by space in SCRIPT_USED
echo -e "SCRIPT USED: $TEMPO_SCRIPT_USED\n"
echo -e "SCRIPT LOCAL LANGUAGE USED: $LANG\n"
echo -e "JOB ID: $JOB_ID\n"

################ COMMAND DEFINITION

# http://stackoverflow.com/questions/255898/how-to-iterate-over-arguments-in-bash-script

usage () {
cat << EOF

USAGE: `basename $0` -c config_file.conf 

OPTIONS:
    -h    help
    -c    name and path of the config .conf file. Optional if rogge_12231.conf is in /pasteur/homes/gmillot/rogge12231/
    -r     --remove_tm_files        logical option: Remove temporary files ? Only 0 or 1 admitted. Default true (0).
    
EX1:  `basename $0`  -c /pasteur/homes/gmillot/rogge12231/

EOF
}

while getopts ":hc:r:" OPTION ; do
# add : after the option name to specify that something is required (-h has nothing required after)
# the first : before h is to induce getopts switching to "silent error reporting mode" (disable annoying messages).
    case $OPTION in 
        h)    usage; exit 1 ;; 
        c)    verif_fun $OPTARG ; CONFIG_FILE=$OPTARG  ;;
        r)    verif_fun $OPTARG ; REMOVE_TMP=$OPTARG  ;;
        \?)  echo -e "### ERROR ### INVALID OPTION: - $OPTARG\n"  ;  usage; exit 1;;
        :)  echo "### ERROR ### OPTION -$OPTARG REQUIRES AN ARGUMENT\n" >&2 usage; exit 1;;
        esac
done
shift $((OPTIND-1))
LOCAL_USER_VAR+=" CONFIG_FILE REMOVE_TMP" # do not forget the space before the variable name


################ CHECK

######## CONF FILE CHECKING

if [[ ! -z $CONFIG_FILE ]] ; then
    if [[ ! -f $CONFIG_FILE ]]; then 
        echo -e "\n### ERROR ###  -c OPTION (config file) MISSING AT THE INDICATED PATH: $CONFIG_FILE\n"
        usage
        exit 1
    fi
else
    CONFIG_FILE=/pasteur/homes/gmillot/rogge12231/rogge_12231.conf
    single_path_with_regex_fun $CONFIG_FILE
    TEMPO_RETURN_1=$(echo $?) # test space
    if [[ $TEMPO_RETURN_1 == 1  || $TEMPO_RETURN_1 == 2 ]]; then
        echo -e "\n### ERROR ###  -PROBLEM WITH THE single_path_with_regex_fun FUNCTION\n"
        exit 1
    elif [[ $TEMPO_RETURN_1 == 3 ]]; then
        echo -e "\n### ERROR ###  -c OPTION (config file): $CONFIG_FILE NOT PRESENT IN `dirname $CONFIG_FILE`\n"
        usage
        exit 1
    elif [[ $TEMPO_RETURN_1 == 0 ]]; then
        CONFIG_FILE=$(echo $CONFIG_FILE) # to remove regex in the path
    else
        echo -e "\n### ERROR ###  PROBLEM WITH THE single_path_with_regex_fun FUNCTION: RETURN IS OTHER THAN 0-3 VALUES\n"
        exit 1
    fi
fi

source $CONFIG_FILE

#### check the files and variables necessary for the process .sh file

# check alias. Beware: TEST_ALIAS used later to create a tempo alias file for TARS
TEST_ALIAS="R_conf" # put space between strings if several to test
for i in $TEST_ALIAS ; do
   if [[ ! $(type -t $i) == "alias" ]] ; then
        echo -e "\n### ERROR ###  $i ALIAS NOT AVAILABLE IN THE GLOBAL ENVIRONMENT: CHECK IN ${CONFIG_FILE}\n"
        usage
        exit 1
    else
        echo -e "$i ALIAS FROM ${CONFIG_FILE} DETECTED IN THE GLOBAL ENVIRONMENT"
    fi
done

CONF_SCRIPTS=(
    # SCRIPTS
    "r_main_functions_conf"
    "bash_main_functions_conf"
    "r_main_conf"
    "r_compil_conf"
    )
conf_scripts_Num=$(( ${#CONF_SCRIPTS[@]} - 1 )) # total number of elements in the array
LOCAL_USER_VAR+=" CONF_SCRIPTS conf_scripts_Num " # do not forget the space before the variable name

CONF_PATH=(
        # R PARAMETERS - locations
        "PATH_LIB_CONF"
        "PATH_IN_CONF"
        "PATH_OUT_CONF"
    )
conf_path_Num=$(( ${#CONF_PATH[@]} - 1 )) # total number of elements in the array
LOCAL_USER_VAR+=" CONF_PATH conf_path_Num" # do not forget the space before the variable name

CONF_VAR_CHECK=(
    # R PARAMETERS
    "FILE_NAME1_CONF"
    "ML_BOOTSTRAP_NB_CONF"
    "LOOP_NB_CONF"
    "R_RANDOM_SEED"
    "PROJECT_NAME_CONF"
    "LABEL_SIZE"
    "R_OPT_TXT_CONF"
    # SLURM PARAMETERS
    "MAX_RUNNING_TIME_CONF"
    "NB_CPU_PER_TASK_CONF"
    "MEM_PER_CPU_CONF"
    "MAIL_CONF"
    "DEDICATED_CONF"
    "QOS_CONF"
    # SCRIPTS
    $(echo ${CONF_SCRIPTS[@]})
    # R PARAMETERS - locations
    $(echo ${CONF_PATH[@]})
)
conf_var_check_Num=$(( ${#CONF_VAR_CHECK[@]} - 1 )) # total number of elements in the array
LOCAL_USER_VAR+=" CONF_VAR_CHECK conf_var_check_Num" # do not forget the space before the variable name

# verif presence of the conf variables
for ((i=0; i<=$conf_var_check_Num; i++)); do
    var_existence_check_fun ${CONF_VAR_CHECK[$i]}
    TEMPO_RETURN_1=$(echo $?) # test space
    if [[ $TEMPO_RETURN_1 == 1  || $TEMPO_RETURN_1 == 2 ]]; then
        echo -e "\n### ERROR ###  -PROBLEM WITH THE var_existence_check_fun FUNCTION\n"
        exit 1
    elif [[ $TEMPO_RETURN_1 == 3 ]]; then
        echo -e "\n### ERROR ### ${CONF_VAR_CHECK[$i]} VARIABLE IS NECESSARY. CHECK THE config_tars_workflow.conf FILE\n"
        exit 1
    elif [[ $TEMPO_RETURN_1 == 0 ]]; then
        shopt -s extglob # -s unable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
        echo -e "${CONF_VAR_CHECK[$i]} CORRECTLY DETECTED IN config_tars_workflow.conf FILE: ${!CONF_VAR_CHECK[$i]}"
        shopt -u extglob # -u disable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
    else
        echo -e "\n### ERROR ###  PROBLEM WITH THE var_existence_check_fun FUNCTION: RETURN IS OTHER THAN 0-3 VALUES\n"
        exit 1
    fi
done

# verif presence of the special conf variables
for ((i=0; i<=$conf_scripts_Num; i++)); do
    TEMPO_FILE_NAME=${CONF_SCRIPTS[$i]}
    # echo "TEMPO_FILE_NAME IS: $TEMPO_FILE_NAME"
    single_path_with_regex_fun $(echo ${!TEMPO_FILE_NAME})
    TEMPO_RETURN_2=$(echo $?) # test space
    if [[ $TEMPO_RETURN_2 == 1  || $TEMPO_RETURN_2 == 2 ]]; then
            echo -e "\n### ERROR ###  -PROBLEM WITH THE single_path_with_regex_fun FUNCTION\n"
            exit 1
    elif [[ $TEMPO_RETURN_2 == 3 ]]; then
            shopt -s extglob # -s unable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
            echo -e "\n### ERROR ###  PROBLEM WITH $TEMPO_FILE_NAME PARAMETER VALUE NOT CORRECT $(echo ${!TEMPO_FILE_NAME})\nCHECK THE $CONFIG_FILE FILE\n"
            shopt -u extglob # -u disable global extention
            exit 1
    elif [[ $TEMPO_RETURN_2 == 0 ]]; then
            shopt -s extglob # -s unable global extention, ie the recognition of special global pattern in path, like [[:digit:]]
            echo -e "THE $TEMPO_FILE_NAME FUNCTION IMPORTED IS $(echo ${!TEMPO_FILE_NAME})"
            export $TEMPO_FILE_NAME=$(echo ${!TEMPO_FILE_NAME}) # to remove regex in the path
            shopt -u extglob # -u disable global extention
    else
            echo -e "\n### ERROR ###  PROBLEM WITH THE single_path_with_regex_fun FUNCTION: RETURN IS OTHER THAN 0-3 VALUES\n"
            exit 1
    fi
done
echo -e "\n"

#### end check the files and variables necessary for the process .sh file

source $bash_main_functions_conf
# files below necessary for the process .sh file
for i in "show_time_fun" ; do
   if [[ ! $(type -t $i) == "function" ]] ; then
        echo -e "\n### ERROR ###  $i FUNCTION NOT AVAILABLE IN THE GLOBAL ENVIRONMENT: CHECK IN ${CONFIG_FILE}\n"
        usage
        exit 1
    else
        echo -e "$i FUNCTION FROM ${CONFIG_FILE} DETECTED IN THE GLOBAL ENVIRONMENT"
    fi
done
echo -e "\n"

# ckeck the correct values

if [[ ! $R_ANALYSIS_KIND =~ longit|valid_boot|full_cross_validation ]]; then
    echo -e "\n\n### ERROR ### INVALID SETTING FOR R_ANALYSIS_KIND\nONLY longit, valid_boot OR full_cross_validation AUTHORIZED: $R_ANALYSIS_KIND\nCHECK IN $CONFIG_FILE\n\n"
    usage
    exit 1
else
    echo -e "\n\nFOLLOWING R ANALYSIS WILL BE EXECUTED: $R_ANALYSIS_KIND\n\n"
fi
if [[ $R_ANALYSIS_KIND =~ longit && $LOOP_NB_CONF != 1 ]]; then
    LOOP_NB_CONF=1
    echo -e "\n\nR_ANALYSIS_KIND PARAMETER SET TO $R_ANALYSIS_KIND: LOOP_NB_CONF PARAMETER RESET TO 1\n\n"
fi
echo -e "\n\nALL CHECKS OK: PROCESS ENGAGED......................\n\n"


################ END CHECK


################ sbatch LOOP

OUTPUT_DIR_PATH_tempo="${PATH_OUT_CONF}${PROJECT_NAME_CONF}_${JOB_ID}"
mkdir ${OUTPUT_DIR_PATH_tempo}
for ((i=1; i<=$LOOP_NB_CONF; i++)); do 
	mkdir ${OUTPUT_DIR_PATH_tempo}/loop$i
done

# create the tampo alias file for TARS
ALIAS_FILE="${OUTPUT_DIR_PATH_tempo}/tempo_alias_${JOB_ID}.txt"
> ${ALIAS_FILE} ; # empty file created
for i in $TEST_ALIAS ; do
        # echo $i >> ${ALIAS_FILE}
        echo -e "alias $(alias $i )\n" >> ${ALIAS_FILE} ; # beware: space before ) is very important
        # DANGER: in sh SCRIPT I have to use this : echo -e "alias $(alias $i )\n" >> ${ALIAS_FILE} ;
        # But on TARS DIRECTLY typing, I have to use echo -e "$(alias $i )\n" >> ${ALIAS_FILE} ;
done

# CONFIG_FILE=$CONFIG_FILE, # !!!!!!!!!!!!!!!! DANGER NEVER PUT THE CONFIG_FILE IN SUP_VAR FOR SOURCE AGAIN BECAUSE THE CONFIG FILE CAN HAVE BEEN MODIFIED FOR ANOTHER JOB !! PUT BY HAND ALL THE DESIRED VARIABLES IN SUP_VAR
# R_conf, #do not work because it is an alias
# all these in SUP_VAR will be injected into the TARS job
SUP_VAR="JOB_ID=$JOB_ID"
SUP_VAR+=",REMOVE_TMP=$REMOVE_TMP"
# For sbatch script
SUP_VAR+=",OUTPUT_DIR_PATH_tempo=$OUTPUT_DIR_PATH_tempo"
SUP_VAR+=",ALIAS_FILE=$ALIAS_FILE"
# for R script args
SUP_VAR+=",r_main_conf=${r_main_conf}"
SUP_VAR+=",PATH_LIB_CONF=$PATH_LIB_CONF"
SUP_VAR+=",PATH_IN_CONF=$PATH_IN_CONF"
SUP_VAR+=",r_main_functions_conf=$r_main_functions_conf"
SUP_VAR+=",FILE_NAME1_CONF=$FILE_NAME1_CONF"
SUP_VAR+=",ML_BOOTSTRAP_NB_CONF=$ML_BOOTSTRAP_NB_CONF"
SUP_VAR+=",PROJECT_NAME_CONF=$PROJECT_NAME_CONF"
SUP_VAR+=",LABEL_SIZE=$LABEL_SIZE"
SUP_VAR+=",R_OPT_TXT_CONF=$R_OPT_TXT_CONF"
SUP_VAR+=",R_ANALYSIS_KIND=$R_ANALYSIS_KIND"
SUP_VAR+=",CROSS_VALID_RATIO=$CROSS_VALID_RATIO"
SUP_VAR+=",R_RANDOM_SEED=$R_RANDOM_SEED"

LOCAL_USER_VAR+=" OUTPUT_DIR_PATH_tempo SUP_VAR" # do not forget the space before the variable name

COUNT=1
while [[ $COUNT -lt $(($LOOP_NB_CONF + 1)) ]] ; do # is less than
    SUP_VAR_tempo="$SUP_VAR,COUNT=$COUNT"
    if [[ $R_ANALYSIS_KIND =~ valid_boot && $COUNT == "1" ]] ; then
    	echo -e '#!/bin/sh
    		# write the previous line exactly like this, with no comments, otherwise do not work
    		# source $CONFIG_FILE # !!!!!!!!!!!!!!!! DANGER NEVER SOURCE AGAIN BECAUSE THE CONFIG FILE CAN HAVE BEEN MODIFIED FOR ANOTHER JOB !! PUT BY HAND ALL THE DESIRED VARIABLES IN SUP_VAR
    		OUTPUT_DIR_PATH_tempo2="${OUTPUT_DIR_PATH_tempo}/loop${COUNT}/"
    		# next line cannot be put outside (which would have been convenient -> put into the SUP_VAR_tempo for display. But SUP_VAR_tempo for sbatch do not like spaces)
    		echo -e "\nSBATCH CORRESPONDING TO R_ANALYSIS_KIND =~ valid_boot && COUNT == 1\n"
    		source $ALIAS_FILE # recover the alias
            R_PROC="R_conf ${r_main_conf} $PATH_LIB_CONF $PATH_IN_CONF ${OUTPUT_DIR_PATH_tempo2} $r_main_functions_conf $FILE_NAME1_CONF $ML_BOOTSTRAP_NB_CONF $PROJECT_NAME_CONF $LABEL_SIZE $R_OPT_TXT_CONF $COUNT $R_ANALYSIS_KIND $CROSS_VALID_RATIO $R_RANDOM_SEED"
    		R_PROC2="${R_PROC} &> ${OUTPUT_DIR_PATH_tempo2}loop${COUNT}_r_console_messages.txt"  # or "$R_PROC > ${OUTPUT_DIR_PATH_tempo2}loop${COUNT}_r_console_messages.txt 2>&1" # to add the estderror in the stdout
    		eval "$R_PROC2"
    	' | sbatch -p $DEDICATED_CONF --job-name=wait_loop1_${JOB_ID} --wait --qos $QOS_CONF --time $MAX_RUNNING_TIME_CONF -c $NB_CPU_PER_TASK_CONF --mem-per-cpu $MEM_PER_CPU_CONF --mail-type END,FAIL --mail-user $MAIL_CONF --export $SUP_VAR_tempo | tee -a ${OUTPUT_DIR_PATH_tempo}/loop${COUNT}/loop${COUNT}_${PROJECT_NAME_CONF}_slurm_jobID.txt # write all th echo from the $PROC alaso into a log file
    	# tricky part of this sbatch because the  SUP_VAR_tempo will be used in the script piped to sbatch
    	((COUNT=COUNT + 1))
    	# echo -e "\nAFTER SBATCH CORRESPONDING TO R_ANALYSIS_KIND =~ valid_boot && COUNT == 1 COUNT = $COUNT"
    elif [[ $R_ANALYSIS_KIND =~ valid_boot && $COUNT > "1" ]] ; then  
		echo -e '#!/bin/sh
        	echo "LOOP 1 HAS CORRECTLY BEEN WAIT FOR END\n"
        ' | sbatch --dependency=singleton -p $DEDICATED_CONF --job-name=wait_loop1_${JOB_ID} --wait --qos $QOS_CONF --time 1 -c 1 --mem-per-cpu 10M --mail-type END,FAIL --mail-user $MAIL_CONF --export $SUP_VAR_tempo
		echo -e '#!/bin/sh
    		# write the previous line exactly like this, with no comments, otherwise do not work
            # source $CONFIG_FILE # !!!!!!!!!!!!!!!! DANGER NEVER SOURCE AGAIN BECAUSE THE CONFIG FILE CAN HAVE BEEN MODIFIED FOR ANOTHER JOB !! PUT BY HAND ALL THE DESIRED VARIABLES IN SUP_VAR
    		OUTPUT_DIR_PATH_tempo2="${OUTPUT_DIR_PATH_tempo}/loop${SLURM_ARRAY_TASK_ID}/"
    		# next line cannot be put outside (which would have been convenient -> put into the SUP_VAR_tempo for display. But SUP_VAR_tempo for sbatch do not like spaces)
    		echo -e "\nSBATCH CORRESPONDING TO R_ANALYSIS_KIND =~ valid_boot && COUNT > 1\n"
            source $ALIAS_FILE # recover the alias
    		R_PROC="R_conf ${r_main_conf} $PATH_LIB_CONF $PATH_IN_CONF ${OUTPUT_DIR_PATH_tempo2} $r_main_functions_conf $FILE_NAME1_CONF $ML_BOOTSTRAP_NB_CONF $PROJECT_NAME_CONF $LABEL_SIZE $R_OPT_TXT_CONF ${SLURM_ARRAY_TASK_ID} $R_ANALYSIS_KIND $CROSS_VALID_RATIO $R_RANDOM_SEED" # beware $COUNT replaced by ${SLURM_ARRAY_TASK_ID} because job array
    		R_PROC2="${R_PROC} &> ${OUTPUT_DIR_PATH_tempo2}loop${SLURM_ARRAY_TASK_ID}_r_console_messages.txt"  # or "$R_PROC > ${OUTPUT_DIR_PATH_tempo2}loop${SLURM_ARRAY_TASK_ID}_r_console_messages.txt 2>&1" # to add the estderror in the stdout
    		eval "$R_PROC2"
    	' | sbatch -p $DEDICATED_CONF --array=2-$LOOP_NB_CONF --job-name=wait_loop_all_${JOB_ID} --wait --qos $QOS_CONF --time $MAX_RUNNING_TIME_CONF -c $NB_CPU_PER_TASK_CONF --mem-per-cpu $MEM_PER_CPU_CONF --mail-type END,FAIL --mail-user $MAIL_CONF --export $SUP_VAR_tempo | tee -a $(for((i = 2 ; i <= $LOOP_NB_CONF ; i++)) ; do echo ${OUTPUT_DIR_PATH_tempo}/loop${i}/loop${i}_${PROJECT_NAME_CONF}_slurm_jobID.txt ; done)  # tee is dispached in all the dir of the job array
    	# tricky part of this sbatch because the  SUP_VAR_tempo will be used in the script piped to sbatch
		COUNT=$(($LOOP_NB_CONF + 1))
		# echo -e "\nAFTER SBATCH CORRESPONDING TO R_ANALYSIS_KIND =~ valid_boot && COUNT > 1 COUNT = $COUNT"
	else
		echo -e '#!/bin/sh
    		# write the previous line exactly like this, with no comments, otherwise do not work
            # source $CONFIG_FILE # !!!!!!!!!!!!!!!! DANGER NEVER SOURCE AGAIN BECAUSE THE CONFIG FILE CAN HAVE BEEN MODIFIED FOR ANOTHER JOB !! PUT BY HAND ALL THE DESIRED VARIABLES IN SUP_VAR
    		OUTPUT_DIR_PATH_tempo2="${OUTPUT_DIR_PATH_tempo}/loop${SLURM_ARRAY_TASK_ID}/"
    		# next line cannot be put outside (which would have been convenient -> put into the SUP_VAR_tempo for display. But SUP_VAR_tempo for sbatch do not like spaces)
    		echo -e "\nSBATCH CORRESPONDING TO ELSE\n"
            source $ALIAS_FILE # recover the alias
            R_PROC="R_conf ${r_main_conf} $PATH_LIB_CONF $PATH_IN_CONF ${OUTPUT_DIR_PATH_tempo2} $r_main_functions_conf $FILE_NAME1_CONF $ML_BOOTSTRAP_NB_CONF $PROJECT_NAME_CONF $LABEL_SIZE $R_OPT_TXT_CONF ${SLURM_ARRAY_TASK_ID} $R_ANALYSIS_KIND $CROSS_VALID_RATIO $R_RANDOM_SEED"
    		R_PROC2="${R_PROC} &> ${OUTPUT_DIR_PATH_tempo2}loop${SLURM_ARRAY_TASK_ID}_r_console_messages.txt"  # or "$R_PROC > ${OUTPUT_DIR_PATH_tempo2}loop${SLURM_ARRAY_TASK_ID}_r_console_messages.txt 2>&1" # to add the estderror in the stdout
    		eval "$R_PROC2"
    	' | sbatch -p $DEDICATED_CONF --array=1-$LOOP_NB_CONF --job-name=wait_loop_all --wait --qos $QOS_CONF --time $MAX_RUNNING_TIME_CONF -c $NB_CPU_PER_TASK_CONF --mem-per-cpu $MEM_PER_CPU_CONF --mail-type END,FAIL --mail-user $MAIL_CONF --export $SUP_VAR_tempo | tee -a $(for((i = 1 ; i <= $LOOP_NB_CONF ; i++)) ; do echo ${OUTPUT_DIR_PATH_tempo}/loop${i}/loop${i}_${PROJECT_NAME_CONF}_slurm_jobID.txt ; done) # tee is dispached in all the dir of the job array
    	# tricky part of this sbatch because the  SUP_VAR_tempo will be used in the script piped to sbatch
    	COUNT=$(($LOOP_NB_CONF + 1))
    	# echo -e "\nAFTER SBATCH CORRESPONDING TO ELSE COUNT = $COUNT"
	fi
done
echo -e '#!/bin/sh
    echo "ALL LOOPS HAVE CORRECTLY BEEN WAIT FOR END\n"
' | sbatch --dependency=singleton -p $DEDICATED_CONF --job-name=wait_loop_all_${JOB_ID} --wait --qos $QOS_CONF --time 1 -c 1 --mem-per-cpu 10M --mail-type END,FAIL --mail-user $MAIL_CONF --export $SUP_VAR_tempo

# echo -e "\nAFTER ALL FIRST ROUND OF SBATCH COUNT = $COUNT"
LOCAL_USER_VAR+=" SUP_VAR_tempo" # do not forget the space before the variable name

if [[ $R_ANALYSIS_KIND =~ longit || $LOOP_NB_CONF == "1" ]] ; then
    echo -e "\nNO NEED TO COMPILE DATA SINCE NO LOOP PERFORMED\n"
else
    OUTPUT_DIR_PATH_FINAL="${OUTPUT_DIR_PATH_tempo}/final_res"
    mkdir ${OUTPUT_DIR_PATH_FINAL}
    SUP_VAR+=",r_compil_conf=$r_compil_conf"
    SUP_VAR+=",OUTPUT_DIR_PATH_FINAL=$OUTPUT_DIR_PATH_FINAL"
    SUP_VAR+=",LOOP_NB_CONF=$LOOP_NB_CONF"
    echo -e '#!/bin/sh
        # write the previous line exactly like this, with no comments, otherwise do not work
        # source $CONFIG_FILE # !!!!!!!!!!!!!!!! DANGER NEVER SOURCE AGAIN BECAUSE THE CONFIG FILE CAN HAVE BEEN MODIFIED FOR ANOTHER JOB !! PUT BY HAND ALL THE DESIRED VARIABLES IN SUP_VAR
        # next line cannot be put outside (which would have been convenient -> put into the SUP_VAR_tempo for display. But SUP_VAR_tempo for sbatch do not like spaces)
        echo -e "\nSBATCH CORRESPONDING TO COMPIL\n"
        source $ALIAS_FILE # recover the alias
        R_PROC="R_conf ${r_compil_conf} $PATH_LIB_CONF ${OUTPUT_DIR_PATH_tempo}/ ${OUTPUT_DIR_PATH_FINAL}/ $r_main_functions_conf $PROJECT_NAME_CONF $LABEL_SIZE $R_OPT_TXT_CONF $LOOP_NB_CONF $R_ANALYSIS_KIND"
        R_PROC2="${R_PROC} &> ${OUTPUT_DIR_PATH_FINAL}/r_console_messages.txt" # or "$R_PROC > ${OUTPUT_DIR_PATH_FINAL}/r_console_messages.txt 2>&1" # to add the estderror in the stdout
        eval "$R_PROC2"
    ' | sbatch -p $DEDICATED_CONF --job-name=compil --qos $QOS_CONF --time $MAX_RUNNING_TIME_CONF -c $NB_CPU_PER_TASK_CONF --mem-per-cpu $MEM_PER_CPU_CONF --mail-type END,FAIL --mail-user $MAIL_CONF --export $SUP_VAR | tee -a ${OUTPUT_DIR_PATH_FINAL}/${PROJECT_NAME_CONF}_slurm_jobID.txt # write all th echo from the $PROC alaso into a log file
fi

################ END MAIN CODE

################ LANDING

echo -e "\n\n......................ALL JOBS SUBMITTED\n\n"
STOPTIME=$(date)
LOCAL_USER_VAR+=" STOPTIME" # do not forget the space before the variable name
echo -e "PROCESS END TIME:" $STOPTIME
echo -e "JOB LAPSE TIME: " $(show_time_fun $(($(date -d"$STOPTIME" +%s) - $(date -d"$STARTTIME" +%s)))) # +%s to print the date in seconds
SCRIPT_USED=$(echo $SCRIPT_USED | sed 's/__/\ /g') # replace 2 underscores by space in SCRIPT_USED
echo -e "\n\nVARIABLES USED:\n"
# echo $LOCAL_USER_VAR
IFS=' ' read -ra LOCAL_USER_VAR2 <<< "$LOCAL_USER_VAR" # system that split a character string into an array according to different delimiters (here space)
unset IFS # set IFS back to ist default value
for j in $(seq 0 $(( ${#LOCAL_USER_VAR2[@]} - 1 )) ) ; do # use echo $LOCAL_USER_VAR[@]} to see all the arry and echo ${#LOCAL_USER_VAR[@]} to have the length of the arry
    if [[ ${LOCAL_USER_VAR2[$j]} =~ alias ]] ; then
        type ${LOCAL_USER_VAR2[$j]}
    else
        echo -e "${LOCAL_USER_VAR2[$j]}: $(echo "${!LOCAL_USER_VAR2[$j]}")" # $(eval ${LOCAL_USER_VAR2[$i]})" ;
    fi
done
echo -e "\n\n"
cd $INI_POSITION
module purge








