#### DESCRIPTION

Project 12231 submitted by Lars Rogge, Institut Pasteur, Paris, France
Perfom Limma and Random forest analysis using a discovery cohorte of patients (for model building) and using a validation cohorte for validating the model.
Dedicated to SLURM (TARS CLuster) analysis


#### LICENCE

This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.

#### CREDITS

Vincent Rouilly, Institut Pasteur, Paris, France
Vincent Guillemot, Hub-C3BI, Institut Pasteur, USR 3756 IP CNRS, Paris, France
Gael A. Millot, Hub-C3BI, Institut Pasteur, USR 3756 IP CNRS, Paris, France


#### HOW TO USE IT

Download the desired Tagged version, never the current master.
Open rogge_12231.conf
Set the parameters
Start the job using a command like:
STARTTIME_SH=$(date) ; JOB_ID_SH=$(date -d "$STARTTIME_SH" +%s) ; sh /pasteur/homes/gmillot/Git_versions_to_use/rogge_12231-v6.0.0/rogge_12231_workflow.sh -c /pasteur/homes/gmillot/Git_versions_to_use/rogge_12231-v6.0.0/rogge_12231.conf | tee /pasteur/homes/gmillot/rogge12231/${JOB_ID_SH}_workflow_log &


#### FILE DESCRIPTION

rogge_12231.conf	config file. Parameters need to be set by the user inside this file before running
rogge_12231_workflow.sh	file allowing checkings and SLURM job submission
rogge_12231_main_analysis.R	R script run by SLURM
rogge_12231_data_compilation.R	R script run by SLURM that compil the data from the previous loops
The four files must be in the same directory before running

rogge_12231_manual_graph_adjustment.R	optional R script to run in R, after SLURM job, to improve the representation of the graphs. Open the script and set the parameter inside before running


#### TARS OUTPUT DESCRIPTION

## For each job submitted to SLURM:
workflow_log	messages printed by the rogge_12231_workflow.sh script
slurm.out	nothing relevant

## For each loop (defined by LOOP_NB_CONF in rogge_12231.conf):
graphs	graphs
r_rogge_12231_1549557200_report.txt	full report of the rogge_12231_whole_VGVR.R script
res_data.RData	saved objects
rogge_12231_slurm_jobID.txt	SLURM job ID
r_console_messages.txt	messages printed by the R console


#### WEB LOCATION

Check for updated versions (most recent tags) at https://gitlab.pasteur.fr/gmillot/rogge_12231/tags


#### WHAT'S NEW IN

## v6.0.0

Bug regarding the randomness fixed


## v5.0.0

Gene filtering added


## v4.0.0

Bug fixed for the Limma analysis and for the valid_boot kind of analysis


## v3.0.0

rogge_12231_main_analysis.R file modified to fit with the new 194samples_67training_13replication_normalized_LR20022019.txt data file


## v2.0.0

rogge_12231_manual_graph_adjustment.R file added


## v1.0.0

Everything

